<?xml version="1.0" encoding="utf-8"?>
<section version="5.0"
         xsi:schemaLocation="http://docbook.org/ns/docbook http://docbook.org/xml/5.0/xsd/docbook.xsd"
         xml:id="display_objects.trajectory_lines"
         xmlns="http://docbook.org/ns/docbook"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:xs="http://www.w3.org/2001/XMLSchema"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:xi="http://www.w3.org/2001/XInclude"
         xmlns:ns="http://docbook.org/ns/docbook">
  <title>Trajectory lines</title>

  <para>
    <informalfigure><screenshot><mediaobject><imageobject>
       <imagedata fileref="images/display_objects/trajectory_lines_panel.png" format="PNG" scale="50" />
    </imageobject></mediaobject></screenshot></informalfigure>

    This <link linkend="display_objects">visual element</link> renders a set of continuous lines to visualize the
    trajectories of motion of particles. Typically, this element is created by the
    <link linkend="particles.modifiers.generate_trajectory_lines">Generate trajectory lines</link> modifier.
  </para>

   <simplesect>
    <title>See also</title>
    <para>
      <pydoc-link href="modules/ovito_vis" anchor="ovito.vis.TrajectoryVis"><classname>TrajectoryVis</classname> (Python API)</pydoc-link>
    </para>
   </simplesect>

</section>
