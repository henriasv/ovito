.. OVITO scripting documentation master file

**********************************
OVITO Scripting Manual
**********************************

This manual describes OVITO's scripting interface. You can access this document from the *Help* menu of OVITO.

Introduction:
-----------------

.. toctree::
   :maxdepth: 1
   
   introduction/running
   introduction/overview
   introduction/file_io
   introduction/modifiers
   introduction/particle_properties
   introduction/rendering
   introduction/custom_modifiers
   introduction/advanced_topics
   introduction/examples
   introduction/version_changes

Python Module Reference:
-------------------------

.. toctree::
   
   modules/ovito
   modules/ovito_data
   modules/ovito_io
   modules/ovito_io_ase
   modules/ovito_modifiers
   modules/ovito_pipeline
   modules/ovito_vis
